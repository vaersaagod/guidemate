<?php
namespace vaersaagod\guidemate\assetbundles\guidemate;

use Craft;
use craft\web\AssetBundle;
use craft\web\assets\cp\CpAsset;

class GuideMateAsset extends AssetBundle
{
    // Public Methods
    // =========================================================================

    /**
     * @inheritdoc
     */
    public function init()
    {
        $this->sourcePath = "@vaersaagod/guidemate/assetbundles/guidemate/dist";

        $this->depends = [
            CpAsset::class,
        ];

        $this->js = [
            'js/GuideMate.js',
        ];

        $this->css = [
            'css/GuideMate.css',
        ];

        parent::init();
    }
}
