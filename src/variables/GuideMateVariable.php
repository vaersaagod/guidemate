<?php

namespace vaersaagod\guidemate\variables;

use Craft;
use vaersaagod\guidemate\GuideMate;

class GuideMateVariable
{
    // Public Methods
    // =========================================================================

    public function getName()
    {
        return GuideMate::$plugin->getName();
    }

    public function getSettings()
    {
        return GuideMate::$plugin->getSettings();
    }
    
    public function getSectionId()
    {
        return GuideMate::$plugin->guidemate->getSectionId();
    }
    
    public function renderContent($id)
    {
        return GuideMate::$plugin->guidemate->renderContentById($id === 'guidemate' ? null : $id);
    }
}
