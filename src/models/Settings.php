<?php

namespace vaersaagod\guidemate\models;

use vaersaagod\guidemate\GuideMate;

use Craft;
use craft\base\Model;

class Settings extends Model
{
    // Public Properties
    // =========================================================================

    /**
     * @var string
     */
    public $pluginNameOverride;

    /**
     * @var string
     */
    public $template;

    /**
     * @var integer
     */
    public $section;

    // Public Methods
    // =========================================================================

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [];
    }
}
