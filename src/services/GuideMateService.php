<?php
/**
 * GuideMate plugin for Craft CMS 3.x
 *
 * Your friendly guide, mate!
 *
 * @link      https://www.vaersaagod.no
 * @copyright Copyright (c) 2018 Værsågod
 */

namespace vaersaagod\guidemate\services;

use craft\elements\Entry;
use craft\web\View;
use vaersaagod\guidemate\GuideMate;

use Craft;
use craft\base\Component;
use craft\helpers\Template;

/**
 * GuideMateService
 *
 * All of your plugin’s business logic should go in services, including saving data,
 * retrieving data, etc. They provide APIs that your controllers, template variables,
 * and other plugins can interact with.
 *
 * https://craftcms.com/docs/plugins/services
 *
 * @author    Værsågod
 * @package   GuideMate
 * @since     1.0.0
 */
class GuideMateService extends Component
{
    
    public function getSectionId()
    {
        return GuideMate::$plugin->getSettings()->section;
    }
    
    public function renderContentById($id)
    {
        $sectionId = GuideMate::$plugin->getSettings()->section;
        $template = GuideMate::$plugin->getSettings()->template;
        
        $query = Entry::find();
        
        if ($id === null) {
            $criteria = [
                'sectionId' => $sectionId,
            ];
        } else {
            $criteria = [
                'sectionId' => $sectionId,
                'id' => $id,
            ];
        }
        
        Craft::configure($query, $criteria);
        $entry = $query->one();
        
        if (!$entry) {
            return 'an error occured';
        } 
        
        try {
            Craft::$app->view->setTemplateMode(View::TEMPLATE_MODE_SITE);
            $output = Craft::$app->view->renderTemplate($template, [ 'entry' => $entry ]);
            Craft::$app->view->setTemplateMode(View::TEMPLATE_MODE_CP);
        } catch(\yii\base\Exception $e) {
            return 'an error occured';
        } catch(\Twig_Error_Loader $e) {
            return 'an error occured';
        }
        
        return Template::raw($output);
    }
}
