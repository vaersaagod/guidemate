<?php
/**
 * GuideMate plugin for Craft CMS 3.x
 *
 * Your friendly guide, mate!
 *
 * @link      https://www.vaersaagod.no
 * @copyright Copyright (c) 2018 Værsågod
 */

/**
 * GuideMate en Translation
 *
 * Returns an array with the string to be translated (as passed to `Craft::t('guidemate', '...')`) as
 * the key, and the translation as the value.
 *
 * http://www.yiiframework.com/doc-2.0/guide-tutorial-i18n.html
 *
 * @author    Værsågod
 * @package   GuideMate
 * @since     1.0.0
 */
return [
    'GuideMate plugin loaded' => 'GuideMate plugin loaded',
];
